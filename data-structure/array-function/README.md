map(array, callback);
filter(array, callback);
reduce(array, callback, startValue);
some(array, callback);
every(array, callback);
