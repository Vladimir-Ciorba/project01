(function() {

	var sameObj = {
		Author: 'Pushkin',
		Title: 'Together',
		Text: 'Hello World'
	}

	function posRequest(sameObj, cb) {

		var url = 'http://localhost:3333/entry';
		var xhr = new XMLHttpRequest();

		xhr.addEventListener('load', transferComplete);
		xhr.addEventListener('error', transferFaild);
		xhr.open('POST', url);

		function transferComplete() {
			cb(this.responseText);
		}

		function transferFaild() {
			cb(this.status + ', ' + this.statusText);
		}
		xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8"); // application/json;charset=UTF-8 ? 
		xhr.send(JSON.stringify(sameObj));

	}

	posRequest(sameObj, function(item) {
		console.log(item);
	});

})();