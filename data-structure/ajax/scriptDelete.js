(function() {

	var sameObj = {
		EntryId: 'EntryId_b88e1a54-9337-40ea-a604-76e30665df64'
	}

	function posRequest(sameObj, cb) {

		var url = 'http://localhost:3333/entry';
		var xhr = new XMLHttpRequest();

		xhr.addEventListener('load', transferComplete);
		xhr.addEventListener('error', transferFaild);
		xhr.open('DELETE', url);

		function transferComplete() {
			cb(this.responseText);
		}

		function transferFaild() {
			cb(this.status + ', ' + this.statusText);
		}
		xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8"); // application/json;charset=UTF-8 ? 
		xhr.send(JSON.stringify(sameObj));

	}

	posRequest(sameObj, function(item) {
		console.log(item);
	});

})();