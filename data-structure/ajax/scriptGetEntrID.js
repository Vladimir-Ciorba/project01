(function() {


	var id = 'EntryId_b88e1a54-9337-40ea-a604-76e30665df64';

	function posRequest(id, cb) {

		var url = 'http://localhost:3333/entry/' + id;
		var xhr = new XMLHttpRequest();

		xhr.addEventListener('load', transferComplete);
		xhr.addEventListener('error', transferFaild);
		xhr.open('GET', url);

		function transferComplete() {
			cb(this.responseText);
		}

		function transferFaild() {
			cb(this.status + ', ' + this.statusText);
		}

		xhr.send();

	}

	posRequest(id, function(item) {
		console.log(item);
	});

})();