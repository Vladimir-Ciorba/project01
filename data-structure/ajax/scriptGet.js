(function() {

	function posRequest(cb) {

		var url = 'http://localhost:3333/entry';
		var xhr = new XMLHttpRequest();

		xhr.addEventListener('load', transferComplete);
		xhr.addEventListener('error', transferFaild);
		xhr.open('GET', url);

		function transferComplete() {
			cb(this.responseText);
		}

		function transferFaild() {
			cb(this.status + ', ' + this.statusText);
		}

		xhr.send();

	}

	posRequest(function(item) {
		console.log(item);
	});

})();